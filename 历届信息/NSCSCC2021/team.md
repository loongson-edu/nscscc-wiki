# NSCSCC2021 团体赛
## 发布包
[nscscc2021\_group\_v0.01.7z](https://caiyun.139.com/m/i?105CfPYZQpjKQ) 提取码：JeS0 解压密码：nscscc2021
## 提交包
初赛提交包：[nscscc2021\_group\_qualifier\_submission.7z](https://caiyun.139.com/m/i?105Cq2riLweFW) 提取码：CUgB 解压密码：nscscc2021

决赛提交包：[nscscc2021\_group\_final\_submission.7z](https://caiyun.139.com/m/i?105CqMrStwG6X) 提取码：QcRd 解压密码：nscscc2021
## 决赛
展示视频：[bilibili](https://m.bilibili.com/space/521339105)

