# NSCSCC2021

## 赛道入口
[团队赛](./team.md) [个人赛](./single.md)

## 培训
1. [第一次赛前培训活动](https://www.bilibili.com/video/BV1kK411w7i5)
2. [第二次赛前培训活动](https://www.bilibili.com/video/BV11L411p7WM)
3. [第三次赛前培训活动](https://www.bilibili.com/video/BV1GK4y1M7C3)
4. [第四次赛前培训活动](https://www.bilibili.com/video/BV1QU4y1g7dM)
5. [第五次赛前培训活动](https://www.bilibili.com/video/BV1L44y1q7RV)
6. [第六次赛前培训活动](https://www.bilibili.com/video/BV1344y1C7fR)
7. [颁奖典礼](https://www.bilibili.com/video/BV1344y1C7fR)
