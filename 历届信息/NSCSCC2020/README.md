# NSCSCC2020
## 团队赛（2020）

资源包： [百度网盘](https://pan.baidu.com/s/1WyG1tP9tPnUWVkLE4T8Jtw)  提取码：6666

初赛提交包：[百度网盘](https://pan.baidu.com/s/1eULhNXq9xKKcrztO_yqqJQ) 提取码：6666

解压密码：nscscc2020

竞赛设备：[“龙芯” 体系结构与CPU设计教学实验系统](http://loongson.cn/business/general2/jiaoxue/jiaoxueshiyanxiang/2015/11/356.html)

## 个人赛（2020）

资源包: [清华大学云盘](https://cloud.tsinghua.edu.cn/d/ee6e2c2688264ca09b85/ )

## 培训（2020）
第一次线上培训：[IC创新学院](https://www.iccollege.cn/portal/courseDetail/342.mooc)
第二次线上培训：[IC创新学院](https://www.iccollege.cn/portal/courseDetail/343.mooc)

### 决赛-线上比赛

[IC创新学院](https://www.iccollege.cn/live/home/show/85.mooc)

1. 大赛团体赛和个人是题目公布
2. 大赛现场画面

3. 中科院教授包云岗讲座--面向下一代计算的开源芯片与敏捷设计实践

4. Xilinx专家罗霖讲座--统一软件平台Vitis及Vitis AI介绍

### 颁奖典礼

[IC创新学院](https://www.iccollege.cn/live/home/show/96.mooc)

1. 领导致辞
2. 龙芯公司报告

3. 个人赛颁奖

4. 团队赛颁奖

5. 系统能力专家组领导总结发言 

## 团队赛

### 开源代码

| 参赛队伍           | 开源链接                                                     | 编程语言               | 简介                                                         |
| ------------------ | ------------------------------------------------------------ | ---------------------- | ------------------------------------------------------------ |
| UltraMIPS          | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=SocialistDalao&repo=UltraMIPS_NSCSCC)](https://github.com/SocialistDalao/UltraMIPS_NSCSCC) | Verilog                | 拥有详细开发文档和友好型代码的来源作品:基于双发射处理器的UltraMIPS系统设计 |
| 咸鱼们的搬砖体力战 | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=Superscalar-HIT-Core&repo=SHIT-Core-NSCSCC2020)](https://github.com/Superscalar-HIT-Core/SHIT-Core-NSCSCC2020) | SystemVerilog          | 基于MIPS指令集的乱序四发射的超标量处理器                     |
| 正经做CPU1队       | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=14010007517&repo=2020NSCSCC)](https://github.com/14010007517/2020NSCSCC) | Verilog                | 尽可能的挖掘五级流水的潜力                                   |
| 这道题我真的做不队 | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=easter-mips&repo=nscscc2020)](https://github.com/easter-mips/nscscc2020) | SystemVerilog、Chisel3 | 顺序双发五级流水带cache                                      |
| Untitled           | https://code.aliyun.com/SOL/UntitledUnit_FinalSrc.git        | SystemVerilog          | MIPS-10段流水线                                              |
| FDU1.2             | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=TwistsOfFate&repo=vanilla-cpu)](https://github.com/TwistsOfFate/vanilla-cpu) | SystemVerilog          | 性能不俗的五级单发流水线                                     |
| FDU1.1             | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=NSCSCC-2020-Fudan&repo=FDU1.1-NSCSCC)](https://github.com/NSCSCC-2020-Fudan/FDU1.1-NSCSCC) | SystemVerilog          | 一个朴素的顺序双发射MIPS处理器，八级流水；两级流水的Cache    |
| 西北工业大学2队    | https://pan.baidu.com/s/1FrhZBR-2ViAkt_wyKxO7zg 提取码:1234  | Bluespec SystemVerilog | 经典五级流水线                                               |
| 华东师范大学一队   | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=amadeus-mips&repo=amadeus-mips)](https://github.com/amadeus-mips/amadeus-mips) | Chisel                 | Chisel3生成的MIPS32 CPU                                      |
| 我是谁这是哪里队   | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=trifling-mips&repo=trifling-mips)](https://github.com/trifling-mips/trifling-mips )              | SystemVerilog          | 经典单发射五级流水线CPU                                      |

### 展示视频及答辩PPT

2020龙芯杯团队赛决赛作品及答辩PPT：[IC创新学院](https://www.iccollege.cn/study/unit/3019.mooc)

## 个人赛

### 开源代码

| 参赛选手 | 开源链接                                                     | 编程语言     | 简介                                                         |
| -------- | ------------------------------------------------------------ | ------------ | ------------------------------------------------------------ |
| 葛启丰   | https://gitee.com/kevenge/GeMIPS                             | Verilog      | 2020计算机系统能力大赛个人赛参赛代码，五级流水线，能够通过功能测试和性能测试。 |
| 高子博   | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=cassuto&repo=yamp-32)](https://github.com/cassuto/yamp-32) | Verilog、C++ | 五级流水; 分支预测; Cache; StoreBuffer; 周期精确体系结构模拟器 |
| 倪仁涛   | [![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=fluctlight001&repo=cpu_for_nscscc2020)](https://github.com/fluctlight001/cpu_for_nscscc2020) | Verilog      | 伪·顺序双发射七级流水                                        |
| 陆俊峰   | (整理中······)                                               | Verilog      | 五级流水，65mhz，sti魔改成了位计数指令                       |
