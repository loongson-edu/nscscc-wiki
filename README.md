# “龙芯杯”全国大学生计算机系统能力培养大赛——信息汇总

## 竞赛总则

全国大学生计算机系统能力培养大赛（以下简称“大赛”）是由教育部高等学校计算机类专业教学指导委员会和系统能力培养研究专家组共同发起，以学科竞赛
推动专业建设和计算机领域创新人才培养体系改革、培育我国高端芯片及核心系统的技术突破与产业化后备人才为目标，面向高校大学生举办的全国性大赛。
大赛旨在选拔未来我国计算机系统的设计、分析、优化与应用人才，激发学生的想象力、创新力和工程实践能力并培养其团队协作精神，以赛促学、以赛促教，
为高质量专业人才搭建交流、展示、合作的平台，助力我国高校与企业产学研合作的健康快速发展。

大赛官网：[www.nscscc.com](http://www.nscscc.com)

| NSCSCC                             | 成绩                 | 资源包                | 培训                 | 答辩PPT              | 开源代码               |
|------------------------------------|--------------------|--------------------|--------------------|--------------------|--------------------|
| [NSCSCC2023](./历届信息/NSCSCC2023)    | :x:                | :heavy_check_mark: | :heavy_check_mark: | :x:                | :x:                |
| [NSCSCC2022](./历届信息/NSCSCC2022)    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :x:                | :heavy_check_mark: |
| [NSCSCC2021](./历届信息/NSCSCC2021)    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| [NSCSCC2020](./历届信息/NSCSCC2020)    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| [NSCSCC2019](./历届信息/NSCSCC2019.md) | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| [NSCSCC2018](./历届信息/NSCSCC2018)    | :x:                | :x:                | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| [NSCSCC2017](./历届信息/NSCSCC2017)    | :x:                | :x:                | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |

## 经验分享

[第一届-参赛经验分享-清华](./历届信息/NSCSCC2018/PPT/第一届-参赛经验分享-清华.pdf)

[第一届-指导老师经验分享-南航](./历届信息/NSCSCC2018/PPT/第一届-指导老师经验分享-南航.pdf)

[第二届-myCPU启动Linux指南-国科大](https://github.com/loongson-education/nscscc-wiki/blob/master/历届信息/NSCSCC2018/第二届-myCPU启动Linux指南-国科大.pdf)

[第二届-“龙芯杯”系统能力培养大赛心得总结-北理工](https://github.com/cnyangkun/nscscc2018/blob/master/DOC/系统能力培养大赛参赛心得总结-北理工.pdf) :star:

[第三届-针对参加龙芯杯的若干建议-北理工](https://github.com/Silverster98/bit_nscscc_suggestion)

[龙芯比赛vivado调试试错手记](https://blog.csdn.net/zerolord/article/details/107144556)

## 学习资料（望贡献）

**简单五级流水** [一步一步写MIPS CPU](https://github.com/lvyufeng/step_into_mips)  **视频** [教你写一个简单的CPU](https://www.bilibili.com/video/BV1pK4y1C7es/) （重庆大学）

**Cache** [CPU Cache支持](./doc/A14_CPU%20Cache%E6%94%AF%E6%8C%81.pdf)

**TLB** [CPU TLB MMU支持](./doc/A13_CPU%20TLB%20MMU%E6%94%AF%E6%8C%81.zip)

**外围器件** https://www.fpga4fun.com/

**SRAM** [A Practical Introduction to SRAM Memories Using an FPGA](https://www.hackster.io/salvador-canas/a-practical-introduction-to-sram-memories-using-an-fpga-i-3f3992)

**理论 教材** [计算机体系结构基础（第3版）](https://foxsen.github.io/archbase) 胡伟武等 著

**理论 视频** [2018年计算机体系结构课程导教班](https://www.bilibili.com/video/BV17E411W7NS)

**Verilog** [Verilog Tutorial](http://asic-world.com/verilog/veritut.html)

**Tutorial** [Verilog, Formal Verification and Verilator Beginner's Tutorial](https://zipcpu.com/tutorial/)

## Vivado

下载链接：https://www.xilinx.com/support/download.html

Vivado文档：https://www.xilinx.com/products/design-tools/vivado.html#documentation 

## 常用链接

**QQ群号** 583344130

